import unittest
import os

if __name__ == '__main__':
    dir_path = os.path.dirname(os.path.realpath(__file__))
    tests = unittest.TestLoader().discover(dir_path)
    testRunner = unittest.runner.TextTestRunner()
    result = testRunner.run(tests)

    if not result.wasSuccessful():
        exit(1)