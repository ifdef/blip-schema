import unittest
import simplejson as json
from jsonschema import (
    validate,
    ValidationError,
)
import os 

class TestBlip(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        with open(dir_path + '/../../v1/blip/client_info.json', 'r') as f:
            schema_data = f.read()
            self.schema = json.loads(schema_data)
        super(TestBlip, self).__init__(*args, **kwargs)

    def setUp(self):
        self.json_data = {
            "origin": "BLIP",
            "type": "CLIENT_INFO",
            "version": 1,
            "data": {
                "msg": "connected"
            }
        }

    def test_valid(self):
        validate(self.json_data, self.schema)

    def test_invalidVersion(self):
        json_new = self.json_data
        json_new["version"] = 2
        with self.assertRaises(ValidationError):
            validate(json_new, self.schema)
        json_new["version"] = 0
        with self.assertRaises(ValidationError):
            validate(json_new, self.schema)

    def test_invalidOrigin(self): 
        json_new = self.json_data
        json_new["origin"] = "B"
        with self.assertRaises(ValidationError):
            validate(json_new, self.schema)
    
    def test_invalidType(self): 
        json_new = self.json_data
        json_new["type"] = "CLIENT_INFOO"
        with self.assertRaises(ValidationError):
            validate(json_new, self.schema)
   
    def test_invalidData(self): 
        json_new = self.json_data
        json_new["data"]["msg"] = "msg"
        with self.assertRaises(ValidationError):
            validate(json_new, self.schema)

if __name__ == '__main__':
    unittest.main()